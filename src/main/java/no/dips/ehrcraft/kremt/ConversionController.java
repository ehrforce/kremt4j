package no.dips.ehrcraft.kremt;

public interface ConversionController {

    void addValidationMessage(String key, String message);
}
