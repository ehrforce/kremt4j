package no.dips.ehrcraft.kremt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KremtApplication {

    public static void main(String[] args) {
        SpringApplication.run(KremtApplication.class, args);
    }

}
