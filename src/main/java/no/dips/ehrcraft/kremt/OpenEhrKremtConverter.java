package no.dips.ehrcraft.kremt;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @param <T> - the openEHR object
 * @param <U> - the KREMT object
 */
public class OpenEhrKremtConverter<T,U>{
    private final Function<T,U> fromOpenEhr;
    private final Function<U,T> fromKremt;

    public OpenEhrKremtConverter(final Function<T,U> fromOpenEhr, final Function<U,T> fromKremt){
        this.fromOpenEhr = fromOpenEhr;
        this.fromKremt = fromKremt;
    }

    public final U convertFromOpenEhr(final T openEHR){
        return fromOpenEhr.apply(openEHR);
    }
    public final T convertFromKremt(final U kremt){
        return fromKremt.apply(kremt);
    }

    public final List<U> createFromOpenEHrs(final Collection<T> openEHRs){
        return openEHRs.stream().map(this::convertFromOpenEhr).collect(Collectors.toList());
    }

    public final List<T> createFromKremts(final Collection<U> kremts){
        return kremts.stream().map(this::convertFromKremt).collect(Collectors.toList());
    }
}
