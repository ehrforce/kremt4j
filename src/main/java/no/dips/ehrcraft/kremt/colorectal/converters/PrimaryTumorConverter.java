package no.dips.ehrcraft.kremt.colorectal.converters;

import no.dips.ehrcraft.kremt.ConversionException;
import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.simplehr.rm.CodedText;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PrimaryTumorConverter extends OpenEhrKremtConverter<CodedText, String> {

    private static final Map<String, String> COLON_LOCATION_MAP;
    static {
        Map<String, String> m = new HashMap<>();
        // Used in primærtumo
        m.put("at0002", "1"); // Colon (C18-C19)
        m.put("at0003", "2"); //Rectum (C20)

        // Used in specific colon code
        m.put("at0006", "180");
        m.put("at0007", "181");
        m.put("at0008", "182");
        m.put("at0009", "183");
        m.put("at0010", "184");
        m.put("at0011", "185");
        m.put("at0012", "186");
        m.put("at0013", "187");
        m.put("at0014", "188");
        m.put("at0015", "189");
        m.put("at0016", "199");

        COLON_LOCATION_MAP = Collections.unmodifiableMap(m);
    }

    public PrimaryTumorConverter() {
        super(PrimaryTumorConverter::createFromCodedText, PrimaryTumorConverter::createFromString);

    }

    private static String createFromCodedText(CodedText t) {
        if (t == null) {
            return null;
        }
        String result = COLON_LOCATION_MAP.get(t.getCodeString());
        if (result != null) {
            return result;
        } else {
            throw new ConversionException("NO_PRIMARY_TUMOR_CODE", "no primary tumor for coded text: " + t);
        }

    }

    private static CodedText createFromString(String kremtCode) {

        Optional<Map.Entry<String, String>> atCode = COLON_LOCATION_MAP.entrySet().stream().filter(n -> kremtCode.contentEquals(n.getValue())).findFirst();
        if (atCode.isPresent()) {
            return CodedText.builder().codeString(atCode.get().getKey()).build();
        } else {
            throw new ConversionException("NO_KREMTCODE", "No KREMT code found for " + kremtCode);
        }

    }



}
