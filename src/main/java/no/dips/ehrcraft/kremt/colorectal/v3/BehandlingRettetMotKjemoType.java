//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.02 at 01:53:56 PM CEST 
//


package no.dips.ehrcraft.kremt.colorectal.v3;



import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BehandlingRettetMot3Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BehandlingRettetMot3Type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="1"/>
 *     &lt;enumeration value="2"/>
 *     &lt;enumeration value="3"/>
 *     &lt;enumeration value="4"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BehandlingRettetMot3Type")
@XmlEnum
public enum BehandlingRettetMotKjemoType {

    @XmlEnumValue("1")
    PRIMAR_SYKDOM_LOKAL_AVANSERT("1"),
    @XmlEnumValue("2")
    PRIMAR_SYKDOM_FJERNSPREDNING("2"),
    @XmlEnumValue("3")
    TILBAKEFALL_LOKAL("3"),
    @XmlEnumValue("4")
    TILBAKEFALL_FJERNSPREDNING("4"),

    UNKNOWN("999")

    ;

    private final String value;



    BehandlingRettetMotKjemoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BehandlingRettetMotKjemoType fromValue(String v) {
        for (BehandlingRettetMotKjemoType c: BehandlingRettetMotKjemoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
