package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.kremt.colorectal.v3.Behandling;
import no.dips.ehrcraft.simplehr.rm.CodedText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Slf4j
public class MedikamenterConverter extends OpenEhrKremtConverter<List<CodedText>, Behandling.Kjemoterapi.Medikamenter> {

    private static final String Kapecitabin = "at0383";
    private static final  String SFU = "at0385";
    private static final  String Kalsiumfolinat = "at0387";
    private static final  String Oksaliplatin = "at0389";
    private static final  String Irinotecan = "at0391";
    private static final  String VEGF_hemmer = "at0393";
    private static final  String EGFR_hemmer = "at0395";
    private static final  String Annet = "at0397";
    public MedikamenterConverter(){
        super(MedikamenterConverter::toKremt, MedikamenterConverter::toOpenEhr);
    }

    private static Behandling.Kjemoterapi.Medikamenter toKremt(List<CodedText> list){
        Behandling.Kjemoterapi.Medikamenter m = new Behandling.Kjemoterapi.Medikamenter();
        m.setKalsiumfolinat(false);
        m.setKapecitabin(false);
        m.setEGFRHemmer(false);
        m.setVEGFHemmer(false);
        m.setOksaliplatin(false);
        m.setFemFu(false);
        for(CodedText t: list){
            if(t.getCodeString() != null){
                switch (t.getCodeString()){
                    case Kapecitabin:
                        m.setKapecitabin(true);
                        break;
                    case SFU:
                        m.setFemFu(true);
                        break;
                    case Kalsiumfolinat:
                        m.setKalsiumfolinat(true);
                        break;
                    case Oksaliplatin:
                        m.setOksaliplatin(true);
                        break;
                    case VEGF_hemmer:
                        m.setVEGFHemmer(true);
                        break;
                    case EGFR_hemmer:
                        m.setEGFRHemmer(true);
                        break;
                    case Irinotecan:
                        m.setIrinotecan(true);
                        break;
                    case Annet:
                        m.setAnnetMedikamenter(true);
                        break;
                        default:
                            log.warn("Not supported atocode: " + t);
                            break;
                }
            }
        }
        return m;
    }
    private static List<CodedText> toOpenEhr(Behandling.Kjemoterapi.Medikamenter m){
        if(m == null) {
            return Collections.emptyList();
        }else{
            List<CodedText> l  = new ArrayList<>();
            if(m.isAnnetMedikamenter()) {
                l.add(CodedText.fromCodeString(Annet));
            }

            if(m.isEGFRHemmer()){
                l.add(CodedText.fromCodeString(EGFR_hemmer));
            }
            if(m.isVEGFHemmer()){
                l.add(CodedText.fromCodeString(VEGF_hemmer));
            }
            if(m.isFemFu()){
                l.add(CodedText.fromCodeString(SFU));
            }
            if(m.isIrinotecan()){
                l.add(CodedText.fromCodeString(Irinotecan));

            }
            if(m.isKalsiumfolinat()){
                l.add(CodedText.fromCodeString(Kalsiumfolinat));
            }
            if(m.isKapecitabin()){
                l.add(CodedText.fromCodeString(Kapecitabin));
            }
            if(m.isOksaliplatin()){
                l.add(CodedText.fromCodeString(Oksaliplatin));
            }

            return l;
        }
    }


}
