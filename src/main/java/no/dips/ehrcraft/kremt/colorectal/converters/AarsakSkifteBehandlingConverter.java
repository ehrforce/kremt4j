package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.kremt.colorectal.v3.Behandling;
import no.dips.ehrcraft.simplehr.rm.CodedText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Slf4j
public class AarsakSkifteBehandlingConverter extends OpenEhrKremtConverter<List<CodedText>, Behandling.Kjemoterapi.AarsakSkifteBeh> {
    private static final String BIVIRKNING = "at0418";
    private static final String PROGRESJON = "at0420";
    private static final String ANNET = "at0422";
    private static final String UKJENT = "at0424";
    public AarsakSkifteBehandlingConverter(){
        super(AarsakSkifteBehandlingConverter::toKremt, AarsakSkifteBehandlingConverter::toOpenEhr);
    }

    private static final List<CodedText> toOpenEhr(Behandling.Kjemoterapi.AarsakSkifteBeh k){
        if(k == null){
            return Collections.emptyList();
        }
        List<CodedText> l = new ArrayList<>();
        if(k.isAnnetAarsakSkifteBeh()){
            l.add(coded(ANNET));
        }
        if(k.isBivirkningerToksisitet()){
            l.add(coded(BIVIRKNING));
        }
        if(k.isProgresjon()){
            l.add(coded(PROGRESJON));
        }
        return l;
    }

    private static final Behandling.Kjemoterapi.AarsakSkifteBeh toKremt(List<CodedText> list){
        if(list == null){
            return null;
        }
        if(list.size() <= 0){
            return null;
        }
        Behandling.Kjemoterapi.AarsakSkifteBeh b = new Behandling.Kjemoterapi.AarsakSkifteBeh();
        b.setAnnetAarsakSkifteBeh(false);
        b.setProgresjon(false);
        b.setBivirkningerToksisitet(false);
      for(CodedText c: list){
          if(c == null || c.getCodeString() == null){
              log.warn("CodeString is null - " + c);

          }else {
              switch (c.getCodeString()) {
                  case BIVIRKNING:
                      b.setBivirkningerToksisitet(true);
                      break;
                  case PROGRESJON:
                      b.setProgresjon(true);
                      break;
                  case ANNET:
                      b.setAnnetAarsakSkifteBeh(true);
                      break;
                  case UKJENT:
                      log.warn("Ukjent skifte av behandling er ikke supportert");
                      //TODO fiks
                      //b.setAarsakSkifteBehUkjent();

                      break;
                  default:
                      log.warn("Unknown atCode for change of treatmen: " + c.getCodeString());
                      break;

              }
          }
      }
      return b;
    }
    private static CodedText coded(String atcode){
        return CodedText.builder().codeString(atcode).build();
    }
}
