package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.simplehr.rm.CodedText;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class EcogConverter extends OpenEhrKremtConverter<CodedText, String> {

    private static final Map<String, String> ECOG_MAP ;
    static {
        Map<String,String> m = new HashMap<>();
        m.put("at0002", "1");
        m.put("at0003", "2");
        m.put("at0004", "3");
        m.put("at0005", "4");
        m.put("at0006", "5");
        m.put("at0007", "99");
        ECOG_MAP = Collections.unmodifiableMap(m);
    }
    public EcogConverter(){
        super(EcogConverter::toKremt, EcogConverter::toOpenEhr);
    }
    private static final String toKremt(CodedText t){
        if(t == null || t.getCodeString() == null){
            log.warn("ECOG status is not set from coded text" + t);
            return null;
        }else{
            String atCode = ECOG_MAP.get(t.getCodeString());
            if(atCode != null){
                return  atCode;
            }else{
                log.warn("Could not find ECOG kremt code from : " + t);
                return null;
            }
        }
    }
    private static final CodedText toOpenEhr(String s){
        if(s == null){
            return null;
        }
        Optional<Map.Entry<String, String>> atCode = ECOG_MAP.entrySet().stream().filter(n -> s.contentEquals(n.getValue())).findFirst();
        if(atCode.isPresent()){
            return CodedText.fromCodeString(atCode.get().getKey());
        }else{
            log.warn("Could not find atCode for kremt code: "+ s);
            return null;
        }
    }
}
