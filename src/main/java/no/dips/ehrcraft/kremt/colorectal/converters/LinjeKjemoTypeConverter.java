package no.dips.ehrcraft.kremt.colorectal.converters;

import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.kremt.colorectal.v3.LinjeKjemoType;
import no.dips.ehrcraft.simplehr.rm.CodedText;

public class LinjeKjemoTypeConverter extends OpenEhrKremtConverter<CodedText, LinjeKjemoType> {



    public LinjeKjemoTypeConverter(){
        super(LinjeKjemoTypeConverter::convertToKremt, LinjeKjemoTypeConverter::convertToOpenEhr);

    }
    private static final LinjeKjemoType convertToKremt(CodedText codedText){
        if(codedText == null){
            return null;
        }
        String atCode = codedText.getCodeString();
        if (atCode == null) {
            return null;
        } else {
            switch (atCode) {
                case "at0371":
                    return LinjeKjemoType.LINE_1_PALLIATIVE;
                case "at0372":
                    return LinjeKjemoType.LINE_2_PALLIATIVE;
                case "at0373":
                    return LinjeKjemoType.LINE_3_PALLIATIVE;
                case "at0374":
                    return LinjeKjemoType.LATER_PALLIATIVE;
                default:
                    return null;
            }
        }

    }
    private static final CodedText convertToOpenEhr(LinjeKjemoType linjeKjemoType){
        String atCode = getAtCode(linjeKjemoType);
        if(atCode != null){
            return CodedText.builder().codeString(atCode).build();
        }else{
            return null;
        }

    }
    private static final String getAtCode(LinjeKjemoType k){
        switch (k){
            case LINE_1_PALLIATIVE:
                return "at0371";
            case LINE_2_PALLIATIVE:
                return "at0372";
            case LINE_3_PALLIATIVE:
                return "at0373";
            case LATER_PALLIATIVE:
                return "at0374";
                default:
                    return null;
        }
    }
}
