package no.dips.ehrcraft.kremt.colorectal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.OccurencesEnum;

import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import no.dips.ehrcraft.simplehr.rm.*;

import java.util.List;

@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@Archetype(archetype = "openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1")
public class BindingKjemoBean {
    @Path(path = "/data[at0001]/events[at0002]/time", rmType = RmType.DV_DATE_TIME)
    private DvDateTime observationTime;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0339]")
    private CodedText behandlingFor;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0402]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> lokaliseringMetastaserList;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0001]/")
    private CodedText lokalisering;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0004]")
    private CodedText specificLocation;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0355]")
    private CodedText purposeWithTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_ecog_funksjonsstatus_dips.v1]/items[at0001]")
    private CodedText ecogStatus;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0001]/", rmType = RmType.DV_QUANTITY)
    private Quantity cea;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0002]/", rmType = RmType.DV_BOOLEAN)
    private DvBoolean ceaTaken;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0369]", rmType = RmType.DV_DATE_TIME)
    private DvDateTime startTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0370]")
    private CodedText treatmentLine;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0381]/items[at0417]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> reasonForChangedTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0352]", rmType = RmType.DV_TEXT)
    private DvText reasonForOtherChangedTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0380]/items[at0382]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> medication;


}
