package no.dips.ehrcraft.kremt.colorectal;

import lombok.Data;

import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.rm.CodedText;

@Data
@Archetype(archetype = "openEHR-EHR-OBSERVATION.kreftmelding_kolorektal_utredning_dips.v1")
public class ColoRectalUtredningBean {

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]")
    private CodedText funnIUtredning;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0537]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0001]")
    private CodedText lokalisering;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_ecog_funksjonsstatus_dips.v1]/items[at0001]")
    private CodedText ecogStatus;
}
