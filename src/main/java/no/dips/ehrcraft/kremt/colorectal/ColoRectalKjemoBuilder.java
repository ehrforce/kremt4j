package no.dips.ehrcraft.kremt.colorectal;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import no.dips.ehrcraft.kremt.ConversionController;
import no.dips.ehrcraft.kremt.colorectal.converters.*;
import no.dips.ehrcraft.kremt.colorectal.v3.*;
import no.dips.ehrcraft.simplehr.rm.CodedText;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ColoRectalKjemoBuilder implements ConversionController {

    private final BindingKjemoBean bindingKjemoBean;



    public static ColoRectalKjemoBuilder instance(BindingKjemoBean bean) {
        return new ColoRectalKjemoBuilder(bean);
    }


    public Map<String, String> validationErrors = new HashMap<>();

    @Override
    public void addValidationMessage(String key, String message) {
        log.warn("Validation error (key=" + key + ") " + message);
        validationErrors.put(key, message);
    }

    public boolean hasValidationErrors() {
        return validationErrors.size() > 0;
    }

    public Map<String, String> getValidationErrors() {
        return validationErrors;
    }



    public ColoRectalKjemoBuilder(BindingKjemoBean bindingKjemoBean) {

        this.bindingKjemoBean = bindingKjemoBean;
    }

    public KliniskColorectal buildMessage() {
        KliniskColorectal msg = new KliniskColorectal();
        val ecogConverter = new EcogConverter();

        msg.setWHOStatus(ecogConverter.convertFromOpenEhr(bindingKjemoBean.getEcogStatus()));
        msg.setBehandling(getBehandling());
        msg.setLokalisasjonPrimaertumor(getLokalisasjonPrimaerTumor());
        msg.setMeldingsinformasjon(getMeldingsInformasjon());
        msg.setNavigering(getNavigering());


        // Sett riktig meldingstype
        //Hvis BehandlingRettetMot3 = 1 eller 2, settes MeldingsNr = 300 og hvis BehandlingRettetMot3 = 3 eller 4, settes MeldingsNr = 350
        BehandlingRettetMotKjemoType behandlingRettetMot3 = msg.getBehandling().getBehandlingRettetMot().getBehandlingRettetMot3();
        if(BehandlingRettetMotKjemoType.PRIMAR_SYKDOM_LOKAL_AVANSERT == behandlingRettetMot3 || BehandlingRettetMotKjemoType.PRIMAR_SYKDOM_FJERNSPREDNING == behandlingRettetMot3){
            msg.getNavigering().getMeldingsvalg().setMeldingsNr("300");
        }else{
            msg.getNavigering().getMeldingsvalg().setMeldingsNr("350");
        }

        return msg;

    }

    private NavigeringType getNavigering() {
        val nav = new NavigeringType();
        nav.setMeldingstittel("Kolorecetal-KjemoMelding");
        val valg = new NavigeringType.Meldingsvalg();
        /**
         *  100 = Primærsykdom - Utredning, 150 = Tilbakefall - Utredning, 200 = Primærsykdom - Kirurgi
         * 									250 = Tilbakefall - Kirurgi, 300 = Primærsykdom - Kjemoterapi, 350 = Tilbakefall - Kjemoterapi
         */
        valg.setMeldingsNr("300");
        /**
         *
         * 										 1 = Utredning
         * 										 2 = Kirurgi
         * 										 3 = Kjemoterapi-->
         */
        valg.setMeldingstypeSub2("3");


        nav.setMeldingsvalg(valg);
        return nav;
    }

    private MeldingsinformasjonType getMeldingsInformasjon() {
        val m = new MeldingsinformasjonType();
        m.setSkjema("KliniskColorectal");
        m.setVersjonsNr("3.0");
        /**
         * <!-- 0 = Ekstern versjon, 1 = Intern mottak og 2 = Intern papirversjon-->
         */
        m.setVersjonInternEkstern("1");
        return m;
    }

    private Behandling getBehandling() {
        val behandling = new Behandling();
        Behandling.Kjemoterapi kjemoterapi = getKjemoTerapi();


        BehandlingRettetMotKjemoType rettetMotType = getBehandlingRettetMotKjemo();
        if (BehandlingRettetMotKjemoType.UNKNOWN == rettetMotType) {
            addValidationMessage("BEHANDLING_RETTET_MOT", "Behandling rettet mot har feil og ukjent type" + bindingKjemoBean.getBehandlingFor());
        } else {
            Behandling.BehandlingRettetMot rettetMot = new Behandling.BehandlingRettetMot();
            rettetMot.setBehandlingRettetMot3(rettetMotType);
            behandling.setBehandlingRettetMot(rettetMot);
        }

        behandling.setKjemoterapi(kjemoterapi);
        return behandling;
    }

    private Behandling.Kjemoterapi getKjemoTerapi() {
        Behandling.Kjemoterapi kjemoterapi = new Behandling.Kjemoterapi();
        kjemoterapi.setLinjeKjemo(getLinjeKjemoType());
        kjemoterapi.setAarsakSkifteBeh(getAarsakSkifteBeh());
        val medikamentConverter = new MedikamenterConverter();
        kjemoterapi.setMedikamenter(medikamentConverter.convertFromOpenEhr(bindingKjemoBean.getMedication()));
        return kjemoterapi;
    }

    private LinjeKjemoType getLinjeKjemoType() {
        val converter = new LinjeKjemoTypeConverter();
        return converter.convertFromOpenEhr(bindingKjemoBean.getTreatmentLine());

    }

    private BehandlingRettetMotKjemoType getBehandlingRettetMotKjemo() {
        val conv = new BehandlingRettetMotConverter();
        return conv.convertFromOpenEhr(bindingKjemoBean.getBehandlingFor());
    }

    private LokalisasjonPrimaertumor getLokalisasjonPrimaerTumor() {
        val  primaryTumorConverter = new PrimaryTumorConverter();
        String kremtLocalisationCode = primaryTumorConverter.convertFromOpenEhr(bindingKjemoBean.getLokalisering());

        val lokalisasjonPrimaertumor = new LokalisasjonPrimaertumor();

        lokalisasjonPrimaertumor.setLokalisasjonPrimaer(kremtLocalisationCode);
        if ("1".contentEquals(kremtLocalisationCode)) {
          return addSpecifiLocation(lokalisasjonPrimaertumor, primaryTumorConverter);
        }
        return lokalisasjonPrimaertumor;

    }

    private LokalisasjonPrimaertumor addSpecifiLocation(final LokalisasjonPrimaertumor lokalisasjonPrimaertumor, PrimaryTumorConverter converter) {
            String kremtCode = converter.convertFromOpenEhr(bindingKjemoBean.getSpecificLocation());
            if (kremtCode != null) {
                lokalisasjonPrimaertumor.setLokalisasjonPrimaerColon(kremtCode);
            } else {
                addValidationMessage("NO_PRIMARY_COLON_CODE_FOUND", "Could not find kremtkode for spcific location" + bindingKjemoBean.getSpecificLocation());

            }
            return lokalisasjonPrimaertumor;
    }

    private Behandling.Kjemoterapi.AarsakSkifteBeh getAarsakSkifteBeh() {
        val converter = new AarsakSkifteBehandlingConverter();
        return converter.convertFromOpenEhr(bindingKjemoBean.getReasonForChangedTreatment());

    }
}
