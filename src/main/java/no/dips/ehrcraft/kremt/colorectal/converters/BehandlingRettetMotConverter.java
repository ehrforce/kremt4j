package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.kremt.OpenEhrKremtConverter;
import no.dips.ehrcraft.kremt.colorectal.v3.BehandlingRettetMotKjemoType;
import no.dips.ehrcraft.simplehr.rm.CodedText;

@Slf4j
public class BehandlingRettetMotConverter extends OpenEhrKremtConverter<CodedText, BehandlingRettetMotKjemoType> {



    public BehandlingRettetMotConverter() {
        super(BehandlingRettetMotConverter::convertToKremt, BehandlingRettetMotConverter::convertToOpenEhr);

    }

    private static BehandlingRettetMotKjemoType convertToKremt(CodedText codedText) {
        if (codedText != null || codedText.getCodeString() != null) {
            switch (codedText.getCodeString()) {
                case "at0340": // Primærsykdom - lokal/lokalavansert inkludert spredning til regionale lymfeknuter
                    return BehandlingRettetMotKjemoType.PRIMAR_SYKDOM_LOKAL_AVANSERT;
                case "at0341": //Primærsykdom - avansert sykdom (fjernspredning)
                    return BehandlingRettetMotKjemoType.PRIMAR_SYKDOM_FJERNSPREDNING;
                case "at0342": //Tilbakefall - lokal/lokalavansert sykdom inkludert spredning til regionale lymfeknuter
                    return BehandlingRettetMotKjemoType.TILBAKEFALL_LOKAL;
                case "at0343": //Tilbakefall - avansert sykdom (fjernspredning)
                    return BehandlingRettetMotKjemoType.TILBAKEFALL_FJERNSPREDNING;
                default:
                    log.warn("Not supported at code for kjemoterapi behandling rettet mot: " + codedText);
                    return BehandlingRettetMotKjemoType.UNKNOWN;
            }
        }
        return BehandlingRettetMotKjemoType.UNKNOWN;
    }

    private static CodedText convertToOpenEhr(BehandlingRettetMotKjemoType code) {
        String atCode = getAtCodeFromEnum(code);
        if (atCode != null) {
            return CodedText.builder().codeString(atCode).build();
        } else {
            return null;
        }

    }

    private static String getAtCodeFromEnum(BehandlingRettetMotKjemoType code) {
        switch (code) {
            case PRIMAR_SYKDOM_LOKAL_AVANSERT:
                return "at0340";
            case PRIMAR_SYKDOM_FJERNSPREDNING:
                return "at0341";
            case TILBAKEFALL_LOKAL:
                return "at0342";
            case TILBAKEFALL_FJERNSPREDNING:
                return "at0343";
            case UNKNOWN:
                return null;

        }
        return null;
    }


}
