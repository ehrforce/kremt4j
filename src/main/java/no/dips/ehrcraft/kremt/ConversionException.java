package no.dips.ehrcraft.kremt;

public class ConversionException extends  RuntimeException {

    private final String key;
    private final String message;

    public ConversionException(String key, String message){
        super(message);
        this.key = key;
        this.message = message;

    }

    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
