//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.05.02 at 01:53:56 PM CEST 
//


package no.dips.ehrcraft.kremt.lunge.kirurgi.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Meldingstittel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MeldingsNr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="(200|299)?"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "meldingstittel",
    "meldingsNr"
})
@XmlRootElement(name = "Navigering")
public class Navigering {

    @XmlElement(name = "Meldingstittel", required = true, defaultValue = "")
    protected String meldingstittel;
    @XmlElement(name = "MeldingsNr", required = true, defaultValue = "")
    protected String meldingsNr;

    /**
     * Gets the value of the meldingstittel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeldingstittel() {
        return meldingstittel;
    }

    /**
     * Sets the value of the meldingstittel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeldingstittel(String value) {
        this.meldingstittel = value;
    }

    /**
     * Gets the value of the meldingsNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeldingsNr() {
        return meldingsNr;
    }

    /**
     * Sets the value of the meldingsNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeldingsNr(String value) {
        this.meldingsNr = value;
    }

}
