package no.dips.ehrcraft.kremt;

import no.dips.ehrcraft.kremt.colorectal.BindingKjemoBean;
import no.dips.ehrcraft.simplehr.DocumentController;
import no.dips.ehrcraft.simplehr.LoadCompositionWithAnnotation;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ColoRectalKjemoBindingTest {

    public DocumentController getControllerForKjemo01(){
        InputStream is = ColoRectalKjemoBindingTest.class.getResourceAsStream("/colorectal/openehr/kjemo01.xml");
        return DocumentController.create(is);
    }

    public DocumentController getControllerForKjemo_02(){
        InputStream is = ColoRectalKjemoBindingTest.class.getResourceAsStream("/colorectal/openehr/kjemo02.xml");
        return DocumentController.create(is);
    }
    @Test
    public void testKjemo01DataIsOk(){
        LoadCompositionWithAnnotation loader = new LoadCompositionWithAnnotation(getControllerForKjemo01());
        Optional<BindingKjemoBean> optionalResult = loader.loadFrom(BindingKjemoBean.class);
        assertTrue(optionalResult.isPresent());
        BindingKjemoBean result = optionalResult.get();
        assertNotNull(result);
        assertEquals("at0340", result.getBehandlingFor().getCodeString());
        assertEquals("at0002", result.getLokalisering().getCodeString());
        assertEquals("at0008", result.getSpecificLocation().getCodeString());
        assertEquals("at0002", result.getEcogStatus().getCodeString());
        assertEquals("0 - Full daglig aktivitet", result.getEcogStatus().getValue());

        assertEquals("at0418", result.getReasonForChangedTreatment().get(0).getCodeString());

        List<CodedText> medications = result.getMedication();
        assertEquals(2, medications.size());
        assertEquals("at0387", medications.get(0).getCodeString());
        assertEquals("at0391", medications.get(1).getCodeString());
    }
    @Test
    public void testKjemo02DataIsOk(){
        LoadCompositionWithAnnotation loader = new LoadCompositionWithAnnotation(getControllerForKjemo_02());
        Optional<BindingKjemoBean> resultOptional = loader.loadFrom(BindingKjemoBean.class);
        assertTrue(resultOptional.isPresent());
        BindingKjemoBean result = resultOptional.get();


        assertEquals(12.0, result.getCea().getMagnitude());
    }
}
