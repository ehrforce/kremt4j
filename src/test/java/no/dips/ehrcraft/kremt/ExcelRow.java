package no.dips.ehrcraft.kremt;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class ExcelRow {
    private String fieldName;
    private String display;
    private String storage;
    private String dataType;
    private String control;
    private String condition;
    private String rules;
    private String mandatory;
    private String xsdPath;
    private String help;

}
