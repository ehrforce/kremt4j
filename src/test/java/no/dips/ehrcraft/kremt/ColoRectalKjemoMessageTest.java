package no.dips.ehrcraft.kremt;

import junitparams.JUnitParamsRunner;
import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.BindingKjemoBean;
import no.dips.ehrcraft.kremt.colorectal.ColoRectalKjemoBuilder;
import no.dips.ehrcraft.kremt.colorectal.v3.Behandling;
import no.dips.ehrcraft.kremt.colorectal.v3.BehandlingRettetMotKjemoType;
import no.dips.ehrcraft.kremt.colorectal.v3.KliniskColorectal;
import no.dips.ehrcraft.kremt.colorectal.v3.LokalisasjonPrimaertumor;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.*;


public class ColoRectalKjemoMessageTest {


    private KliniskColorectal createMessage(BindingKjemoBean bindingKjemoBean) {
        ColoRectalKjemoBuilder builder = new ColoRectalKjemoBuilder(bindingKjemoBean);
        KliniskColorectal msg = builder.buildMessage();
        assertNotNull(msg, "It should return a message");
        return msg;

    }

    protected BindingKjemoBean createDefaultBindingKjemoBean() {
        return BindingKjemoBean.builder()
                .behandlingFor(
                        //Primærsykdom - lokal/lokalavansert inkludert spredning til regionale lymfeknuter
                        CodedText.builder().codeString("at0340").build()
                )
                .lokaliseringMetastaserList(
                        Lists.list(
                                CodedText.builder().codeString("at0405").value("Lunge").build(),
                                CodedText.builder().codeString("at0411").value("Hjerne").build()
                        )
                )
                .lokalisering(
                        //Colon (C18-C19)
                        CodedText.builder().codeString("at0002").build()
                ).specificLocation(
                        //Ascendens (C18.2)
                        CodedText.builder().codeString("at0008").build()
                ).ecogStatus(
                        CodedText.builder().codeString("at0002").build()
                ).reasonForChangedTreatment(
                        Lists.list(
                                CodedText.builder().codeString("at0418").build(),// Bivirkning
                                CodedText.builder().codeString("at0420").build() //Progresjon
                        )
                ).medication(
                        Lists.list(
                                CodedText.fromCodeString("at0383"), //Kapecitabin
                                CodedText.fromCodeString("at0389") // Oksaliplatin
                        )
                ).ecogStatus(CodedText.fromCodeString("at0005"))

                .build();
    }
    @Test
    public void testToXml() throws JAXBException {
        val msg = createMessage(createDefaultBindingKjemoBean());
        JAXBContext ctx = JAXBContext.newInstance(KliniskColorectal.class);
        Marshaller m = ctx.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter w = new StringWriter();
        m.marshal(msg,w);
        String result = w.toString();
        assertNotNull(result);
        System.out.println(result);
    }
    @Test
    public void testEcogStatus(){
        val ecog = createMessage(createDefaultBindingKjemoBean()).getWHOStatus();
        assertNotNull(ecog);
        assertEquals("4", ecog, "3 - Sengeliggende > 50 % av dagtid");
    }

    @Test
    public void testLokalisasjonPrimarTumor() {
        LokalisasjonPrimaertumor lok = createMessage(createDefaultBindingKjemoBean()).getLokalisasjonPrimaertumor();
        assertNotNull(lok);
        assertNotNull(lok.getLokalisasjonPrimaer());
        assertEquals("1", lok.getLokalisasjonPrimaer());

    }

    @Test
    public void testMedication(){
        Behandling.Kjemoterapi.Medikamenter m = createMessage(createDefaultBindingKjemoBean()).getBehandling().getKjemoterapi().getMedikamenter();
        assertTrue(m.isKapecitabin());
        assertTrue(m.isOksaliplatin());
    }

    @Test
    public void testBehandlingFor() {
        Behandling behandling = createMessage(createDefaultBindingKjemoBean()).getBehandling();
        Behandling.BehandlingRettetMot rettetMot = behandling.getBehandlingRettetMot();
        assertNotNull(rettetMot, "Behandling rettet mot skal være satt");
        assertEquals(BehandlingRettetMotKjemoType.PRIMAR_SYKDOM_LOKAL_AVANSERT, rettetMot.getBehandlingRettetMot3());
    }

    @Test
    public void testTransformOne() {
        KliniskColorectal msg = createMessage(createDefaultBindingKjemoBean());
        assertNotNull(msg.getBehandling());
        assertNotNull(msg.getBehandling().getKjemoterapi());
        Behandling.Kjemoterapi kjemoterapi = msg.getBehandling().getKjemoterapi();

        Behandling.Kjemoterapi.AarsakSkifteBeh changedTreatment = kjemoterapi.getAarsakSkifteBeh();
        assertNotNull(changedTreatment);
        assertTrue(changedTreatment.isBivirkningerToksisitet());
        assertTrue(changedTreatment.isProgresjon());
        assertFalse(changedTreatment.isAnnetAarsakSkifteBeh());


    }
}
