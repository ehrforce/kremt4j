package no.dips.ehrcraft.kremt;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Slf4j
public class Member {
    private String path;
    private String type;
    private String name;
    @Builder.Default
    private List<String> values = new ArrayList<>();

    public void addValue(String val) {
        if (values == null) {
            values = new ArrayList<>();
        }
        values.add(val);
    }

    public String getParentName() {
        if (path != null) {
            PathExtractor p = new PathExtractor(path);
            PathExtractor parent = new PathExtractor(p.getParentPath());
            return parent.getChildName();
        } else {
            log.warn("Path is null");
            return null;
        }
    }
    public String getPossibleRmType(){
        if(StringUtils.contains(type, "string")){
            if(isPossibleOrdinal()){
                return "DV_ORDINAL";
            }else{
                return "DV_CODED_TEXT";
            }
        }else{
            return "DV_BOOLEAN";
        }
    }
    private boolean isPossibleOrdinal(){
        if(values != null && values.size() > 0){

            for (String value : values) {
                if(!StringUtils.isNumeric(value)){
                    return false;
                }
            }
            return true;

        }else{
            return false;
        }
    }
}
