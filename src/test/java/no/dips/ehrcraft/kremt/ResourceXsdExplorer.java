package no.dips.ehrcraft.kremt;

import lombok.extern.slf4j.Slf4j;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultText;
import org.w3c.dom.Attr;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ResourceXsdExplorer {
    private final String file;

    public ResourceXsdExplorer(String file) {

        this.file = file;
    }

    public Map<String, Member> explore() throws FileNotFoundException, DocumentException {
        File xsdFile = new File(file);
        if (!xsdFile.exists() || xsdFile.isDirectory()) {
            throw new FileNotFoundException("The XSD file does not exist or is a directory");

        }
        Map<String, String> nsContext = new HashMap<>();
        nsContext.put("xs", "http://www.w3.org/2001/XMLSchema");
        DocumentFactory documentFactory = DocumentFactory.getInstance();
        documentFactory.setXPathNamespaceURIs(nsContext);

        SAXReader reader = new SAXReader(documentFactory);
        Document document = reader.read(xsdFile);
        Element root = document.getRootElement();
        log.info("RootElement: {} ", root.getName());
        String messageName = "";
        Node test = root.selectSingleNode("xs:element/@name");
        if(test != null && test instanceof Attribute){
            Attribute messageNameAttr = (Attribute) test;
            messageName = messageNameAttr.getValue();
        }
        treeWalk(messageName, root);
        return memberMap;
    }

    private Map<String, Member> memberMap = null;

    private void addMember(String path, Member member) {
        if(memberMap == null){
            memberMap = new HashMap<>();
        }
        memberMap.put(path, member);
    }


    public void treeWalk(String path, Element element) {
        String currentPath = path;

        String name = element.getName();
        if ("element".contentEquals(name)) {
            String nameOfElement = element.attributeValue("name");
            currentPath = currentPath + "/" + nameOfElement;
            String xpath = "xs:simpleType/xs:restriction";
            Node testNode = element.selectSingleNode(xpath);
            if (testNode != null && testNode instanceof Element) {
                Element restriction = (Element) testNode;
                String type = restriction.attributeValue("base");
                Member member = new Member(currentPath,  type, nameOfElement, new ArrayList<>());
                if ("xs:string".contentEquals("" + type)) {
                    String xpathEnumeration = "xs:enumeration";
                    List<Node> listOfEnumeration = restriction.selectNodes(xpathEnumeration);
                    for (Node e : listOfEnumeration) {
                        Element ee = (Element) e;
                        String val = ee.attributeValue("value");
                        if (val != null && !val.isEmpty()) {
                            member.addValue(val);
                        }
                    }


                }

                addMember(currentPath, member);




            }


        }
        for (int i = 0, size = element.nodeCount(); i < size; i++) {
            Node node = element.node(i);

            if (node instanceof Element) {
                //log.info("|-- {} - {}", nextLevel,((Element)node).getName());
                treeWalk(currentPath, (Element) node);
            } else {
                // do something…
                if (node instanceof DefaultText) {

                } else {
                    log.info("Next node is not elmenet - it is {}", node.getClass());
                }
            }
        }
    }
}
