package no.dips.ehrcraft.kremt;

enum ColNameEnum {
    FELTNAVN("Feltnavn", 0),
DISPLAYVALUES("Visnignsverdier", 1),
STORAGEVALUE("Lagringsverdi",2),
DATATYPE("Datatype", 3),
CONTROL("Kontroll", 4),
CONDITIONS("Betinget formattering", 5),
RULES("Regler og valideringer", 6),
MANDATORY("Obligatorisk", 7),
XSD("XML Schema", 8),
HELP("Hjelpetektst", 9)

;
private final String colName;
private final int col;

ColNameEnum(String colName, int col){

    this.colName = colName;
    this.col = col;
}

public String getColName() {
    return colName;
}

public int getCol() {
    return col;
}
}
