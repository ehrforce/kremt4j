package no.dips.ehrcraft.kremt;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static no.dips.ehrcraft.kremt.ColNameEnum.*;

@Slf4j
public class ReourceExcelReader {

    private Workbook workbook;

    public ReourceExcelReader(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    private List<ExcelRow> excelRows = new ArrayList<>();

    public List<ExcelRow> getExcelRows() {
        return excelRows;
    }

    private boolean useValidation = false;

    private void add(ExcelRow row) {
        if (useValidation) {
            if (row.getFieldName() == null || row.getFieldName().trim().isEmpty()) {
                row.setFieldName(currentFieldName);

            } else {
                currentFieldName = row.getFieldName();
                currentDatatype = null;
                currentXsdPath = null;

            }

            if (row.getXsdPath() == null || row.getXsdPath().trim().isEmpty()) {
                row.setXsdPath(currentXsdPath);
            } else {
                currentXsdPath = row.getXsdPath();
            }

            if (row.getStorage() == null || row.getStorage().trim().isEmpty()) {
                row.setStorage(currentStorageType);
            } else {
                row.setStorage(currentStorageType);
            }
        }
        excelRows.add(row);

    }

    private String currentStorageType = null;
    private String currentDatatype = null;
    private String currentXsdPath = null;
    private String currentFieldName = null;

    public List<RmElement> loadSheet(String name) {

        return loadSheet(workbook.getSheet(name));
    }

    private List<RmElement> loadSheet(Sheet sheet) {

        int startAt = 2;
        int numRows = sheet.getLastRowNum();
        List<RmElement> elements = new ArrayList<>();
        RmElement element = null;
        for (int i = startAt; i <= numRows; i++) {
            Row r = sheet.getRow(i);
            Optional<String> feltnavnOptional = getStringFromCell(r, FELTNAVN);
            Optional<String> xsdOptional = getStringFromCell(r, XSD);
            Optional<String> dataTypeOptional = getStringFromCell(r, DATATYPE);
            Optional<String> valueOptional = getStringFromCell(r, DISPLAYVALUES);
            Optional<String> storageOptional = getStringFromCell(r, STORAGEVALUE);
            Optional<String> conditionOptional = getStringFromCell(r, CONDITIONS);
            Optional<String> rulesOptional = getStringFromCell(r, MANDATORY);
            Optional<String> help = getStringFromCell(r, HELP);
            if (feltnavnOptional.isPresent()) {
                if (element != null) {
                    //log.info("OLD {}", element);
                    elements.add(element);
                }


                element = new RmElement();
                element.name = feltnavnOptional.get();
                element.xsd = xsdOptional.orElse("NO XSD");
                element.datatype = dataTypeOptional.orElse("N/A");
                if(conditionOptional.isPresent()){
                    element.addProperty(CONDITIONS.name(), conditionOptional.get());
                }
                if(rulesOptional.isPresent()){
                    element.addProperty(MANDATORY.name(), rulesOptional.get());
                }
                if(help.isPresent()){
                    element.addProperty(HELP.name(), help.get());
                }
                if (valueOptional.isPresent()) {
                    String storage = storageOptional.orElse("N/A");
                    if ("false".contentEquals(storage) || "true".contentEquals(storage)) {
                        element.datatype = "boolean";
                        RmField f = new RmField();
                        f.value = valueOptional.get();
                        f.storage = storageOptional.orElse("NA");
                        if(conditionOptional.isPresent()){
                            f.addProperty(CONDITIONS.name(), conditionOptional.get());
                        }
                        if(rulesOptional.isPresent()){
                            f.addProperty(MANDATORY.name(), rulesOptional.get());
                        }
                        if(help.isPresent()){
                            f.addProperty(HELP.name(), help.get());
                        }
                        element.add(f);

                    }
                }

            } else {
                if (element != null) {

                    RmField f = new RmField();
                    f.value = valueOptional.orElse("N/A-VALUE");
                    f.storage = storageOptional.orElse("N/A LAGRINGSVERDI");
                    if(conditionOptional.isPresent()){
                        f.addProperty(CONDITIONS.name(), conditionOptional.get());
                    }
                    if(rulesOptional.isPresent()){
                        f.addProperty(MANDATORY.name(), rulesOptional.get());
                    }
                    if(help.isPresent()){
                        f.addProperty(HELP.name(), help.get());
                    }
                    element.add(f);

                }
            }


        }
        return elements;


    }


    private Optional<String> getStringFromCell(final Row row, final ColNameEnum colNameEnum) {


        Cell cell = row.getCell(colNameEnum.getCol());
        if (cell != null) {

            if (cell.getCellTypeEnum() == CellType.STRING) {
                String val = cell.getStringCellValue();
                log.trace("Row [{}][{}]={}", row.getRowNum(), colNameEnum.getCol(), val);
                return Optional.of(val);
            }else if(cell.getCellTypeEnum() == CellType.NUMERIC){
                 double val = cell.getNumericCellValue();
                 int n = Math.toIntExact(Math.round(val));
                log.trace("Row [{}][{}]={}", row.getRowNum(), colNameEnum.getCol(), n);
                return Optional.of("" + n);
            }
            else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }



    public static ReourceExcelReader create(File file) throws IOException {
        try {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            return new ReourceExcelReader(workbook);
        } catch (FileNotFoundException e) {
            throw new IOException(e);
        }

    }

}
