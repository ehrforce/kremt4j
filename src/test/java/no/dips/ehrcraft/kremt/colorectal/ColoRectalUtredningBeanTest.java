package no.dips.ehrcraft.kremt.colorectal;

import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.converters.EcogConverter;
import no.dips.ehrcraft.kremt.colorectal.converters.PrimaryTumorConverter;
import no.dips.ehrcraft.simplehr.SimplEhrMapper;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ColoRectalUtredningBeanTest {

    @Test
    public void testLoad() throws IOException {

        InputStream is = ColoRectalUtredningBeanTest.class.getResourceAsStream("/colorectal/openehr/utredning01.xml");
        SimplEhrMapper mapper = new SimplEhrMapper();
        Optional<ColoRectalUtredningBean> utredning = mapper.readValue(is, ColoRectalUtredningBean.class);
        assertTrue(utredning.isPresent());
        ColoRectalUtredningBean rectalUtredningBean = utredning.get();

        assertNotNull(rectalUtredningBean.getFunnIUtredning());
        assertEquals("at0450", rectalUtredningBean.getFunnIUtredning().getCodeString());

        assertNotNull(rectalUtredningBean.getLokalisering());
        System.out.println(rectalUtredningBean.getLokalisering());
        CodedText lokalisering = rectalUtredningBean.getLokalisering();
        PrimaryTumorConverter converter = new PrimaryTumorConverter();
        String kremtLokalisering = converter.convertFromOpenEhr(lokalisering);
        System.out.println(lokalisering.getCodeString() +  " -> " + kremtLokalisering);

        CodedText ecog = rectalUtredningBean.getEcogStatus();
        val ecogConvert = new EcogConverter();
        String ecogKremt = ecogConvert.convertFromOpenEhr(ecog);
        System.out.println(ecogKremt);


    }

}