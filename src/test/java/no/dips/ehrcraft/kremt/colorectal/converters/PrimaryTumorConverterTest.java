package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class PrimaryTumorConverterTest {

    /**
     * KliniskColorectal/LokalisasjonPrimaertumor/LokalisasjonPrimaer
     */
    @ParameterizedTest
    @CsvSource(value = {"1:at0002:Colon", "2:at0003:Rectum"}, delimiter = ':')
    public void testPrimaryTumor(String kremtCode, String atCode, String label) {
        val converter = new PrimaryTumorConverter();
        String result = converter.convertFromOpenEhr(CodedText.builder().codeString(atCode).build());
        assertNotNull(result, "Should not be null for : " + atCode);
        assertEquals(kremtCode, result, label);
    }

    /**
     * KliniskColorectal/LokalisasjonPrimaertumor/LokalisasjonPrimaerColon
     *
     * @param label
     * @param kremt
     * @param atCode
     */
    @ParameterizedTest
    @CsvFileSource(resources = "/testdata/primary_tumor_colon.csv", numLinesToSkip = 1)
    public void testAllPrimaryTumorColon(String label, String kremt, String atCode) {
        val converter = new PrimaryTumorConverter();
       String result =  converter.convertFromOpenEhr(CodedText.builder().codeString(atCode).build());
       assertNotNull(result, "Should not be null for: " + atCode);
       assertEquals(kremt, result, label);

    }
}