package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.v3.Behandling;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AarsakSkifteBehandlingConverterTest {

    private Behandling.Kjemoterapi.AarsakSkifteBeh convert(String ... atCode) {
        List<CodedText> list = new ArrayList<>();
        for(String s: atCode){
            list.add(CodedText.fromCodeString(s));
        }
        val converter = new AarsakSkifteBehandlingConverter();
        return converter.convertFromOpenEhr(list);

    }
    private Behandling.Kjemoterapi.AarsakSkifteBeh convert(String atCode){
        CodedText t = CodedText.fromCodeString(atCode);
        val converter = new AarsakSkifteBehandlingConverter();
        Behandling.Kjemoterapi.AarsakSkifteBeh result = converter.convertFromOpenEhr(Lists.list(t));
        assertNotNull(result);
        return result;
    }
    @Test
    public void testBivirkningAndProgresjon(){
        Behandling.Kjemoterapi.AarsakSkifteBeh r = convert("at0418", "at0420");
        assertTrue(r.isBivirkningerToksisitet());
        assertTrue(r.isProgresjon());
        assertFalse(r.isAnnetAarsakSkifteBeh());
    }

    @Test
    public void testBivirkning(){
        //at0418,Bivirkninger/toksisitet
        Behandling.Kjemoterapi.AarsakSkifteBeh result = convert("at0418");
        assertTrue(result.isBivirkningerToksisitet());
        assertFalse(result.isProgresjon());
        assertFalse(result.isAnnetAarsakSkifteBeh());
    }
    @Test
    public void testProgresjon(){
        //at0420,Progresjon
        Behandling.Kjemoterapi.AarsakSkifteBeh result = convert("at0420");
        assertFalse(result.isAnnetAarsakSkifteBeh());
        assertFalse(result.isBivirkningerToksisitet());
        assertTrue(result.isProgresjon());



    }
    @Test
    public void testAnnet(){
        //at0422,Annet
        Behandling.Kjemoterapi.AarsakSkifteBeh r = convert("at0422");
        assertFalse(r.isProgresjon());
        assertFalse(r.isBivirkningerToksisitet());
        assertTrue(r.isAnnetAarsakSkifteBeh());

    }
    @Test
    public void testUkjent(){
//at0424,Ukjent
        Behandling.Kjemoterapi.AarsakSkifteBeh r = convert("at0424");
        assertFalse(r.isProgresjon());
        assertFalse(r.isBivirkningerToksisitet());
        assertFalse(r.isAnnetAarsakSkifteBeh());
    }

    @Test
    public void testNull(){
        val converter = new AarsakSkifteBehandlingConverter();
        val coded = CodedText.fromCodeString(null);

        Behandling.Kjemoterapi.AarsakSkifteBeh r = converter.convertFromOpenEhr(Lists.list(coded));
        assertNotNull(r);
    }

}