package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class EcogConverterTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/testdata/ecog_test_data.csv",  numLinesToSkip = 1)
    public void testEcogData(String kremtCode, String atCode, String label){
        val converter = new EcogConverter();
        String result = converter.convertFromOpenEhr(CodedText.fromCodeString(atCode));
        assertNotNull(result);
        assertEquals(kremtCode, result, label);

    }

}