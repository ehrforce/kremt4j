package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.v3.LinjeKjemoType;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class LinjeKjemoTypeConverterTest {
    @ParameterizedTest
    @CsvSource(
            value = {
                    "at0371:1:1.linje palliativ behandling",
                    "at0372:2:2.linje palliativ behandling",
                    "at0373:3:3.linje palliativ behandling",
                    "at0374:4:Seinere palliativ linje"
            },
            delimiter = ':'
    )
    public void testLinjeKjemo(String atCode, String kremtCode, String label) {
        val converter = new LinjeKjemoTypeConverter();
        LinjeKjemoType result = converter.convertFromOpenEhr(CodedText.builder().codeString(atCode).build());
        assertNotNull(result, "Should not be null for atCode = " + atCode);
        assertEquals(kremtCode, result.value());

    }

}