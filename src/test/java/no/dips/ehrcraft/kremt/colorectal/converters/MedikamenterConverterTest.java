package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.v3.Behandling;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MedikamenterConverterTest {

    private static final String Kapecitabin = "at0383";
    private static final  String SFU = "at0385";
    private static final  String Kalsiumfolinat = "at0387";
    private static final  String Oksaliplatin = "at0389";
    private static final  String Irinotecan = "at0391";
    private static final  String VEGF_hemmer = "at0393";
    private static final  String EGFR_hemmer = "at0395";
    private static final  String Annet = "at0397";

    private Behandling.Kjemoterapi.Medikamenter convert(String ... atCode){
        List<CodedText> l = new ArrayList<>();
        for(String s:atCode){
            l.add(CodedText.fromCodeString(s));

        }
        val converter = new MedikamenterConverter();
        Behandling.Kjemoterapi.Medikamenter m = converter.convertFromOpenEhr(l);
        assertNotNull(m, "Medeikamenter skal ikke være null");
        return m;

    }

    @Test
    public void testMultiple(){
        val m = convert(Kapecitabin, EGFR_hemmer, SFU);
        assertTrue(m.isKapecitabin());
        assertTrue(m.isEGFRHemmer());
        assertFalse(m.isVEGFHemmer());
        assertTrue(m.isFemFu());
        assertFalse(m.isIrinotecan());
        assertFalse(m.isKalsiumfolinat());
        assertFalse(m.isAnnetMedikamenter1());
        assertFalse(m.isAnnetMedikamenter2());
        assertFalse(m.isAnnetMedikamenter3());
        assertFalse(m.isAnnetMedikamenter5());
    }

    @Test
    public void testKapecitabin(){
        val m = convert(Kapecitabin);
        assertTrue(m.isKapecitabin());
        assertFalse(m.isEGFRHemmer());
        assertFalse(m.isVEGFHemmer());
        assertFalse(m.isFemFu());
        assertFalse(m.isIrinotecan());
        assertFalse(m.isKalsiumfolinat());
        assertFalse(m.isAnnetMedikamenter1());
        assertFalse(m.isAnnetMedikamenter2());
        assertFalse(m.isAnnetMedikamenter3());
        assertFalse(m.isAnnetMedikamenter5());
    }

    @Test
    public void testEGFR(){
        val m = convert(EGFR_hemmer);
        assertFalse(m.isKapecitabin());
        assertTrue(m.isEGFRHemmer());
        assertFalse(m.isVEGFHemmer());
        assertFalse(m.isFemFu());
        assertFalse(m.isIrinotecan());
        assertFalse(m.isKalsiumfolinat());
        assertFalse(m.isAnnetMedikamenter1());
        assertFalse(m.isAnnetMedikamenter2());
        assertFalse(m.isAnnetMedikamenter3());
        assertFalse(m.isAnnetMedikamenter5());
    }


}