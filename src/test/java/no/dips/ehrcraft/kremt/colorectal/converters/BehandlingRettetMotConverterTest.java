package no.dips.ehrcraft.kremt.colorectal.converters;

import lombok.val;
import no.dips.ehrcraft.kremt.colorectal.v3.BehandlingRettetMotKjemoType;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class BehandlingRettetMotConverterTest {

    @ParameterizedTest
    @CsvSource({
            "at0340,1, 'Primærsykdom - lokal/lokalavansert inkludert spredning til regionale lymfeknuter'",
            "at0341,2, 'Primærsykdom - avansert sykdom (fjernspredning)'",
            "at0342,3, 'Tilbakefall - lokal/lokalavansert sykdom inkludert spredning til regionale lymfeknuter'",
            "at0343,4, 'Tilbakefall - avansert sykdom (fjernspredning)"
    }
    )
    public void testAllBehandlingRettetMot(String atCode, String kremtCode, String title) {
        val converter = new BehandlingRettetMotConverter();

        BehandlingRettetMotKjemoType result = converter.convertFromOpenEhr(CodedText.builder().codeString(atCode).build());
        assertNotNull(result);
        assertEquals(kremtCode, result.value());

        CodedText c = converter.convertFromKremt(BehandlingRettetMotKjemoType.fromValue(kremtCode));
        assertNotNull(c);
        assertEquals(atCode, c.getCodeString());

    }

}