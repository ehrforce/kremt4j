package no.dips.ehrcraft.kremt;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class RmField {
    String value;
    String storage;
    String conditional;
    String rules;
    String mandatory;
    Map<String,String> meta = new HashMap<>();

    public void addProperty(String key, String val){
        meta.put(key, val);
    }
}
