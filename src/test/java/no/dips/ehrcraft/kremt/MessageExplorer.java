package no.dips.ehrcraft.kremt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.DocumentException;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class MessageExplorer {


    @Test
    public void explore() throws IOException {
        String root = "C:\\Users\\bna\\workspaces\\ckm\\config-products\\ehrcraft_folder\\prod-kreftregister\\dokumentasjon";
        EnumSet<FileVisitOption> opts = EnumSet.of(FOLLOW_LINKS);
        Map<String, List<ValueSet>> results = new HashMap<>();
        try (Stream<Path> walk = Files.walk(Paths.get(root))) {

            List<String> result = walk.map(x -> x.toString())
                    .filter(f -> f.endsWith(".xml"))
                    .filter(f -> !f.contains("Eksempel"))

                    .collect(Collectors.toList());

            result.forEach(s -> {
                ResourceXmlExplorer resourceXmlExplorer = new ResourceXmlExplorer(s);
                try {
                    var list = resourceXmlExplorer.explore();
                    results.put(resourceXmlExplorer.getMessageTitle(), list);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
        writeToExcel("target/kremt.xslx", results);


    }

    protected void writeToExcel(String file, Map<String, List<ValueSet>> valueSets) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet valueset = workbook.createSheet("ValueSet");
        Row header = valueset.createRow(0);
        header.createCell(0).setCellValue("Melding");
        header.createCell(1).setCellValue("Felt");
        header.createCell(2).setCellValue("Verdi");
        header.createCell(3).setCellValue("Beskrivelse");
        int row = 0;
        for (List<ValueSet> messageValueSets : valueSets.values()) {
            for (ValueSet vs : messageValueSets) {
                row++;
                Row r = valueset.createRow(row);
                r.createCell(0).setCellValue(vs.getMessage());
                r.createCell(1).setCellValue(vs.getElement());
                r.createCell(2).setCellValue(vs.getValue());
                r.createCell(3).setCellValue(vs.getDescription());
            }
        }

        File outfile = new File(file);
        FileOutputStream fos = new FileOutputStream(outfile);
        workbook.write(fos);
        workbook.close();

    }

    protected void writeXsdDataToExcel(String outFile, Map<String, Member> memberMap) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("members");
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("name".toUpperCase());
        header.createCell(1).setCellValue("type".toUpperCase());
        header.createCell(2).setCellValue("value".toUpperCase());
        header.createCell(3).setCellValue("parent".toUpperCase());
        header.createCell(4).setCellValue("rmType".toUpperCase());
        header.createCell(5).setCellValue("path".toUpperCase());
        int numColumns = 5;
        sheet.setAutoFilter(new CellRangeAddress(0, 0, 0, numColumns));
        sheet.createFreezePane(0, 1);
        int row = 0;
        for (Member member : memberMap.values()) {
            if(member.getValues() == null || member.getValues().size() <= 0){
                row++;
                Row r = sheet.createRow(row);
                r.createCell(0).setCellValue(member.getName());
                r.createCell(1).setCellValue(member.getType());
                r.createCell(2).setCellValue("N/A");
                r.createCell(3).setCellValue(member.getParentName());
                r.createCell(4).setCellValue(member.getPossibleRmType());
                r.createCell(5).setCellValue(member.getPath());
            }else{

                for (String value : member.getValues()) {
                    row++;
                    Row r = sheet.createRow(row);
                    r.createCell(0).setCellValue(member.getName());
                    r.createCell(1).setCellValue(member.getType());
                    r.createCell(2).setCellValue(value);
                    r.createCell(3).setCellValue(member.getParentName());
                    r.createCell(4).setCellValue(member.getPossibleRmType());
                    r.createCell(5).setCellValue(member.getPath());

                }
            }



        }
        File f = new File(outFile);
        FileOutputStream fos = new FileOutputStream(f);
        workbook.write(fos);
        workbook.close();
    }
    @Test
    public void testOpenExcelSpec() throws IOException {
        String path = "C:\\Users\\bna\\workspaces\\ckm\\config-products\\ehrcraft_folder\\prod-kreftregister\\dokumentasjon\\Lymfom\\LymfomKLL_v3_0";
        String file = "KliniskLymfomKLL_3_0_Spesifikasjon.xlsx";
        String sheet = "NonHodgkin";
        ReourceExcelReader reader = ReourceExcelReader.create(new File(path, file));
        List<RmElement> rows = reader.loadSheet(sheet);
        assertNotNull(rows);
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        FileOutputStream fos = new FileOutputStream(new File("target/test.json"));
        mapper.writeValue(fos, rows);





    }


    @Test
    public void testEvaluateXsd() throws IOException, DocumentException {
        String path = "C:\\Users\\bna\\workspaces\\ckm\\config-products\\ehrcraft_folder\\prod-kreftregister\\dokumentasjon\\Lymfom\\LymfomKLL_v3_0\\KliniskLymfomKLL_v3_0.xsd";
        ResourceXsdExplorer xsdExplorer = new ResourceXsdExplorer(path);
        Map<String, Member> result = xsdExplorer.explore();
        writeXsdDataToExcel("target/kremt.members.xlsx", result);


    }

    @Test
    public void testLoadOneResourceFile() throws DocumentException {
        String path = "C:\\Users\\bna\\workspaces\\ckm\\config-products\\ehrcraft_folder\\prod-kreftregister\\dokumentasjon\\Lymfom\\Behandlingsskjema\\LymfomKLLBehandlingResourceFile_v2_0.xml";
        ResourceXmlExplorer explorer = new ResourceXmlExplorer(path);
        var result = explorer.explore();
        result.stream().forEach(n -> {
            log.info(n.toString());
        });
        log.info("Meldingstittel:" + explorer.getMessageTitle());
    }


}
