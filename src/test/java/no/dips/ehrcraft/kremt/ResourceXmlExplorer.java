package no.dips.ehrcraft.kremt;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ResourceXmlExplorer {
    private final String file;

    private String rootName;
    private String messageTitle;

    public String getMessageTitle() {
        return messageTitle;
    }

    private List<ValueSet> valueSets = new ArrayList<>();

    public ResourceXmlExplorer(String file) {

        this.file = file;
    }

    private ResourceXmlExplorer add(String element, String value, String display) {
        valueSets.add(ValueSet.builder().description(display).value(value).element(element).message(rootName).build());
        return this;
    }

    public List<ValueSet> explore() throws DocumentException {
        File inpuFile = new File(file);
        SAXReader reader = new SAXReader();
        Document doc = reader.read(inpuFile);
        Element root = doc.getRootElement();
        rootName = root.getName();
        messageTitle = rootName;
        treeWalk(0, root);


        return valueSets;

    }

    public void treeWalk(int level, Element element) {
        int nextLevel = level + 1;

        if (element.attribute("display") != null) {
            String display = element.attributeValue("display");
            String value = element.attributeValue("value");
            String name = element.getName();
            if (value == null || value.trim().isEmpty()) {

            } else {
                add(name, value, display);
            }

        }
        if ("MldTittel".contentEquals(element.getName())) {
            messageTitle = element.getStringValue();
        }
        for (int i = 0, size = element.nodeCount(); i < size; i++) {
            Node node = element.node(i);

            if (node instanceof Element) {
                //log.info("|-- {} - {}", nextLevel,((Element)node).getName());
                treeWalk(nextLevel, (Element) node);
            } else {
                // do something…
            }
        }
    }
}
