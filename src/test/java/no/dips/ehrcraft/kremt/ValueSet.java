package no.dips.ehrcraft.kremt;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ValueSet {
    private String message;
    private String element;
    private String value;
    private String description;

}
