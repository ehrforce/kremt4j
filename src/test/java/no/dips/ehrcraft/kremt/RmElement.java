package no.dips.ehrcraft.kremt;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
    public class RmElement {
        String name;
        String xsd;
        String datatype;
        List<RmField> fields = new ArrayList<>();
        Map<String,String> meta = new HashMap<>();

        public void addProperty(String key, String val){
            meta.put(key, val);
        }

       public  void add(RmField f) {
            fields.add(f);
        }
    }
