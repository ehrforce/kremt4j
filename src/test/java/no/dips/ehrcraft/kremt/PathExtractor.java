package no.dips.ehrcraft.kremt;

import org.apache.commons.lang3.StringUtils;

public class PathExtractor {
    private String childPath;
    private String attributeName;
    private String parentPath;
    private String childName;

    public PathExtractor(String path) {
        this.childPath = path;
        invoke();
    }

    public String getChildPath() {
        return childPath;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getParentPath() {
        return parentPath;
    }

    public String getChildName() {
        return childName;
    }

    private void invoke() {
        int k = childPath.indexOf('|');
        attributeName = null;
        if (k >= 0) {
            attributeName = childPath.substring(k + 1);
            childPath = childPath.substring(0, k);
        }


        int i = childPath.lastIndexOf('/');

        parentPath = "/";
        if (i > 0) {
            parentPath = childPath.substring(0, i);
        }
        childName = childPath.substring(i + 1);
        childName = StringUtils.substringBefore(childName, "[");

    }
}