package no.dips.ehrcraft.kremt;

import no.dips.ehrcraft.kremt.colorectal.BindingKjemoBean;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.assertj.core.util.Lists;

public class BindingKjemoBeanGenerator {

    public static BindingKjemoBean getDefaultBindingKjemoBean() {
        return BindingKjemoBean.builder()
                .behandlingFor(
                //Primærsykdom - lokal/lokalavansert inkludert spredning til regionale lymfeknuter
                CodedText.builder().codeString("at0340").build()
                )
                .lokaliseringMetastaserList(
                        Lists.list(
                                CodedText.builder().codeString("at0405").value("Lunge").build(),
                                CodedText.builder().codeString("at0411").value("Hjerne").build()
                        )
                )
                .lokalisering(
                        //Colon (C18-C19)
                        CodedText.builder().codeString("at0002").build()
                ).specificLocation(
                        //Ascendens (C18.2)
                        CodedText.builder().codeString("at0008").build()
                ).ecogStatus(
                        CodedText.builder().codeString("at0002").build()
                ).reasonForChangedTreatment(
                        Lists.list(
                                CodedText.builder().codeString("at0418").build(),// Bivirkning
                                CodedText.builder().codeString("at0420").build() //Progresjon
                        )
                )

                .build();
    }
}
